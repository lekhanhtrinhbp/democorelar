import { Component } from '@angular/core';
import { OpenIdConnectService } from './shared/opent-id-connect.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Demo-CoreLar';
  constructor(private openIdConnectService: OpenIdConnectService) {
    
  }

  onInit(){
    // var path = window.location.pathname;
    // if(path != "/signin-oidc"){
    //   if(!this.openIdConnectService.userAvailable){
    //     this.openIdConnectService.triggerSignIn();
    //   }
    // }
  }
}
