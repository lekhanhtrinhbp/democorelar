import { Component, OnInit } from '@angular/core';
import { OpenIdConnectService } from '../shared/opent-id-connect.service';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-signin-oidc',
  templateUrl: './signin-oidc.component.html',
  styleUrls: ['./signin-oidc.component.css']
})
export class SigninOidcComponent implements OnInit {

  constructor(private router: Router, private openIdConnectService : OpenIdConnectService) { }

  ngOnInit() {
    //this.openIdConnectService.handleCallback();
    //this.router.navigate(['./image-gallery']);
    this.openIdConnectService.userLoad$.subscribe((userLoaded) => {
      if (userLoaded) {
        this.router.navigate(['./']);
      }
      else {
        if (!environment.production) {
          console.log("An error happened: user wasn't loaded.");
        }
      }
    });

    this.openIdConnectService.handleCallback();
  }

}
