import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RequireAuthenticatedUserRouteGuardService } from './shared/require-authenticated-user-route-guard.service';
import { AboutUsComponent } from './about-us/about-us.component';
import { ImageGalleryModule } from './image-gallery/image-gallery.module';
import { SigninOidcComponent } from './signin-oidc/signin-oidc.component';
import { RedirectSilentRenewComponent } from './redirect-silent-renew/redirect-silent-renew.component';

const routes: Routes = [
  { path: 'signin-oidc', component: SigninOidcComponent },
  { path: 'redirect-silentrenew', component: RedirectSilentRenewComponent },
  { path: 'about-us', component: AboutUsComponent },
  { path: '', redirectTo: 'image-gallery', pathMatch: 'full', canActivate: [RequireAuthenticatedUserRouteGuardService]  },
  { path: '**', redirectTo: 'image-gallery', pathMatch: 'full' , canActivate: [RequireAuthenticatedUserRouteGuardService] }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes),
    ImageGalleryModule
  ],
  exports: [RouterModule]
})

export class AppRoutingModule { }
