import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Observable, throwError, of } from 'rxjs';
import { IImage } from './image';
import { catchError, tap, map} from "rxjs/operators"
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ImageGalleryService {
  private imageGalleryUrl = environment.apiUrl + 'images';
  constructor(private httpClient: HttpClient) { 

  }

  getImages(): Observable<IImage[]>{
    return this.httpClient.get<IImage[]>(this.imageGalleryUrl).pipe(
      tap(),
      tap(data => console.log('All: ' + JSON.stringify(data))),
      catchError(this.handleError)
    )
  }

  getImage(id: string): Observable<IImage> {
    if (id === '0') {
      return of(this.initializeImage());
    }
    const url = `${this.imageGalleryUrl}/${id}`;
    return this.httpClient.get<IImage>(url)
      .pipe(
        tap(data => console.log('getImage: ' + JSON.stringify(data))),
        catchError(this.handleError)
      );
  }

  createImage(image: FormData): Observable<IImage> {
    //const headers = new HttpHeaders({ 'Content-Type': 'multipart/form-data' });
    //image.id = null;
    console.table(image);
    return this.httpClient.post<IImage>(this.imageGalleryUrl, image)//, { headers: headers })
      .pipe(
        tap(data => console.log('createImage: ' + JSON.stringify(data))),
        catchError(this.handleError)
      );
  }

  updateProduct(image: FormData): Observable<IImage> {
    //const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    const url = `${this.imageGalleryUrl}/${image.get('id')}`;
    return this.httpClient.put<IImage>(url, image,)// { headers: headers })
      .pipe(
        tap(() => console.log('updateProduct: ' + image.get('id'))),
        // Return the product on an update
        //map(() => image),
        catchError(this.handleError)
      );
  }

  deleteImage(id: string): Observable<{}> {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    const url = `${this.imageGalleryUrl}/${id}`;
    return this.httpClient.delete<IImage>(url, { headers: headers })
      .pipe(
        tap(data => console.log('deleteImage: ' + id)),
        catchError(this.handleError)
      );
  }

  

  private initializeImage(): IImage {
    // Return an initialized object
    return {
      id: '0',
      title: null,
      fileName: null,
      imageUrl: null,
      file: null
    };
  }

  private handleError(err: HttpErrorResponse) {
    // in a real world app, we may send the server to some remote logging infrastructure
    // instead of just logging it to the console
    let errorMessage = '';
    if (err.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      errorMessage = `An error occurred: ${err.error.message}`;
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      errorMessage = `Server returned code: ${err.status}, error message is: ${err.message}`;
    }
    console.error(errorMessage);
    return throwError(errorMessage);
  }
}
