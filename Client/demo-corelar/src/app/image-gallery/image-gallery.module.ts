import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ImageGalleryComponent } from './image-gallery.component';
import { BrowserModule } from '@angular/platform-browser';
import { RequireAuthenticatedUserRouteGuardService } from '../shared/require-authenticated-user-route-guard.service';
import { ImageEditComponent } from './image-edit.component';
import { ReactiveFormsModule } from '@angular/forms';
import { SigninOidcComponent } from '../signin-oidc/signin-oidc.component';
import { RedirectSilentRenewComponent } from '../redirect-silent-renew/redirect-silent-renew.component';

@NgModule({
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    RouterModule.forChild([
      { path: 'image/:id/edit', component: ImageEditComponent },
      { path: 'image-gallery', component: ImageGalleryComponent, canActivate: [RequireAuthenticatedUserRouteGuardService] }
    ])
  ],
  declarations: [
    ImageGalleryComponent,
    ImageEditComponent
  ]
})

export class ImageGalleryModule { }
