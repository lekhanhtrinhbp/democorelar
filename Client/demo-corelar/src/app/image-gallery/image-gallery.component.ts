import { Component, OnInit } from '@angular/core';
import { IImage } from './image';
import { ImageGalleryService } from './image-gallery.service';
import { OpenIdConnectService } from '../shared/opent-id-connect.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-image-gallery',
  templateUrl: './image-gallery.component.html',
  styleUrls: ['./image-gallery.component.css']
})
export class ImageGalleryComponent implements OnInit {
  pageTitle: string = 'Image Gallery';
  images: IImage[];
  errorMessage: string;
  isAdmin: boolean = (this.openIdConnectService.user.profile.role === "Administrator") ;
  
  constructor(private imageService: ImageGalleryService, 
        private openIdConnectService: OpenIdConnectService,
        private router: Router) { }

  ngOnInit(): void {
    this.imageService.getImages().subscribe(
      images =>{
        this.images = images;
      },
      error => this.errorMessage = error
    )
  }

  deleteImage(event, id: string): void {
    if (id === '0') {
      // Don't delete, it was never saved.
      //this.onSaveComplete();
    } else {
      if (confirm(`Really delete the image: ${this.images.find(i => i.id === id).title}?`)) {
        this.imageService.deleteImage(id)
          .subscribe(
            () => this.onSaveComplete(id),
            (error: any) => this.errorMessage = <any>error
          );
      }
    }
  }
  onSaveComplete(id: string): void {
    this.images = this.images.filter(i => i.id !== id);
  }

}
