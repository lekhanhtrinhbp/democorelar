export interface IImage {
    id: string;
    title: string;
    imageUrl: string;
    fileName: string;
    file: any
}
