import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, FormArray, Validators, FormControlName, AbstractControl, ValidatorFn } from '@angular/forms';
import { ImageGalleryService } from './image-gallery.service';
import { IImage } from './image';
import { OpenIdConnectService } from '../shared/opent-id-connect.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { debounceTime } from 'rxjs/operators';
import { FileUploadValidator } from '../shared/file-upload-validator';

@Component({
  selector: 'app-image-edit',
  templateUrl: './image-edit.component.html',
  styleUrls: ['./image-edit.component.css']
})
export class ImageEditComponent implements OnInit {
  pageTitle: string;
  image: IImage;
  errorMessage: string;
  titleMessage: string;
  fileUploadMessage: string;

  //isAdmin: boolean = (this.openIdConnectService.user.profile.role === "Administrator") ;
  imageForm: FormGroup;
  private sub: Subscription;
  
  private validationMessage = {
    title: {
      required: 'Please enter your title',
      minlength: 'Title must be greater than 3 characters'
    },
    file: {
      required: 'Please enter your file'
    }
   
  }
  
  constructor(private fb: FormBuilder, private imageService: ImageGalleryService, 
                      private openIdConnectService: OpenIdConnectService, 
                      private route: ActivatedRoute, private router: Router) { }

  ngOnInit(): void {
    this.imageForm = this.fb.group({
      title: ['', [Validators.required, Validators.minLength(3)]],
      file: [null],
      fileUpload: [null]
    });

    // Read the product Id from the route parameter
    this.sub = this.route.paramMap.subscribe(
      params => {
        const id = params.get('id');
        this.getImage(id);
        this.setFileUploadValidation(id);
      }
    );

    const titleControl = this.imageForm.get('title');
    titleControl.valueChanges.pipe(
      debounceTime(1000)
    ).subscribe(
      value => this.setTitleMessage(titleControl, 'title')
    );
  }

  onFileChange(files: FileList) {
    if (files[0] && files[0].size > 0) {
      this.imageForm.patchValue({
        file: files[0]
      });
    }
  }

  setFileUploadValidation(id: string): void{
    const fileControl = this.imageForm.get("fileUpload");
    if (id === '0') {
        fileControl.setValidators([FileUploadValidator.required]);
    }
    else{
        fileControl.clearValidators();
    }
    fileControl.updateValueAndValidity();
  }
  
  getImage(id: string): void {
    this.imageService.getImage(id)
      .subscribe(
        (image: IImage) => this.displayImage(image),
        (error: any) => this.errorMessage = <any>error
      );
  }

  displayImage(image: IImage): void {
    if (this.imageForm) {
      this.imageForm.reset();
    }
    this.image = image;

    if (this.image.id === '0') {
      this.pageTitle = 'Add Image';
    } else {
      this.pageTitle = `Edit Image: ${this.image.title}`;
    }
    
    // Update the data on the form
    this.imageForm.patchValue({
      title: this.image.title
      
    });
  }

  deleteImage(): void {
    if (this.image.id === '0') {
      // Don't delete, it was never saved.
      this.onSaveComplete();
    } else {
      if (confirm(`Really delete the image: ${this.image.title}?`)) {
        this.imageService.deleteImage(this.image.id)
          .subscribe(
            () => this.onSaveComplete(),
            (error: any) => this.errorMessage = <any>error
          );
      }
    }
  }

  buildFormData(): FormData{
    const form = new FormData();
    form.append('id', this.image.id);
    form.append('title', this.imageForm.get('title').value);
    form.append('file', this.imageForm.get('file').value);
    return form;
  }
  saveImage(): void {
    if (this.imageForm.valid) {
      if (this.imageForm.dirty) {
        //const img = { ...this.image, ...this.imageForm.value };
        
        const img = this.buildFormData();
        if (img.get('id') === '0') {
          this.imageService.createImage(img)
            .subscribe(
              () => this.onSaveComplete(),
              (error: any) => this.errorMessage = <any>error
            );
        } else {
          this.imageService.updateProduct(img)
            .subscribe(
              () => this.onSaveComplete(),
              (error: any) => this.errorMessage = <any>error
            );
        }
      } else {
        this.onSaveComplete();
      }
    } else {
      this.errorMessage = 'Please correct the validation errors.';
    }
  }

  onSaveComplete(): void {
    // Reset the form to clear the flags
    this.imageForm.reset();
    this.router.navigate(['/image-gallery']);
  }

  setTitleMessage(c: AbstractControl, keyMessage: string ): void{
    this.titleMessage ='';
    if ((c.touched || c.dirty) && c.errors ) {
      this.titleMessage = Object.keys(c.errors).map(
        key => this.titleMessage += this.validationMessage[keyMessage][key]
      ).join(' ');
    }
  }
  
}
