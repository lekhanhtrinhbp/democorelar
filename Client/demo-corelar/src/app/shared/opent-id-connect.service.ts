import { Injectable } from '@angular/core';
import { User, UserManager } from 'oidc-client'
import { environment } from 'src/environments/environment';
import { ReplaySubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class OpenIdConnectService {

  private currentUser: User;
  private userManager: UserManager = new UserManager(environment.openIdConnectSettings);

  userLoad$ = new ReplaySubject<boolean>(1);

  constructor() { 
    this.userManager.clearStaleState();

    this.userManager.events.addUserLoaded(user =>
      {
        if(!environment.production){
          console.log('User load:', user);
        }
        this.currentUser = user;
        this.userLoad$.next(true);
      });

      this.userManager.events.addUserUnloaded(user =>
        {
          if(!environment.production){
            console.log('User load:', user);
          }
          this.currentUser = null;
          this.userLoad$.next(false);
        });
  }

  get userAvailable(){
    return this.currentUser != null;
  }

  get user(){
    return this.currentUser;
  }

  triggerSignIn(){
    this.userManager.signinRedirect().then(() => {
      if (!environment.production) {
        console.log('Redirection to signin triggered.');
      }
    });
  }

  handleCallback(){
    this.userManager.signinRedirectCallback().then((user) => {
      if (!environment.production) {
        console.log('Callback after signin handled.', user);
      }
    });
  }

  handleSilentCallback(){
    this.userManager.signinSilentCallback().then((user) => {
      this.currentUser = user;
      if (!environment.production) {
      console.log('Callback after silent signin handled', user);
      }
      });
  }
  

  triggerSignOut() {
    this.userManager.signoutRedirect().then(function (resp) {
      if (!environment.production) {
        console.log('Redirection to sign out triggered.', resp);
      }
    });
  };

}
