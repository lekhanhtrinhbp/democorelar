import { AbstractControl } from "@angular/forms";

export class FileUploadValidator {
    static required(c: AbstractControl): {[key: string]: any} {
        return c.value == null || c.value.length == 0 ? { "required" : true} : null;
    }
}