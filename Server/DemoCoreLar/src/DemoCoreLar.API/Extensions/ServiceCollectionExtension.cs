﻿using AutoMapper;
using DemoCoreLar.API.Authorization;
using DemoCoreLar.API.Entities;
using DemoCoreLar.API.Filters;
using DemoCoreLar.API.Helpers;
using DemoCoreLar.API.Mapper;
using DemoCoreLar.API.Services;
using IdentityServer4.AccessTokenValidation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json.Serialization;
using Swashbuckle.AspNetCore.Swagger;

namespace DemoCoreLar.API.Extensions
{
    public static class ServiceCollectionExtension
    {
        public static void ConfigureApplicationServices(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddAuthorization(option =>
            {
                option.AddPolicy("UserMustBeOwner", policy =>
                {
                    policy.RequireAuthenticatedUser();
                    policy.AddRequirements(new UserMustBeOwnerRequirement(""));
                });
            });

            services.AddMvc(options =>
            {
                options.Filters.Add<OperationCancelledExceptionFilter>();
            })
                .AddJsonOptions(options =>
            {

                options.SerializerSettings.ContractResolver =
                    new CamelCasePropertyNamesContractResolver();
            });

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info { Title = "Image Gallery", Version = "v1" });
            });

            // Configure CORS so the API allows requests from angular
            services.AddCors(options =>
            {
                options.AddPolicy("AllowAllOriginsHeadersAndMethods",
                    builder => builder.AllowAnyOrigin()
                                    .AllowAnyHeader()
                                    .AllowAnyMethod());
            });

            var connectionString = configuration.GetConnectionString("imageGalleryDBConnection");

            services.AddDbContext<ImageGalleryContext>(o => o.UseSqlServer(connectionString));

            services.AddAuthentication(IdentityServerAuthenticationDefaults.AuthenticationScheme)
                .AddIdentityServerAuthentication(option =>
                {
                    option.Authority = "https://localhost:44364/";
                    option.ApiName = "imageGallery-api";
                });
        }
        public static void AddDependencyInjection(this IServiceCollection services)
        {
            services.AddTransient<IImageRepository, ImageRepository>();
            services.AddTransient<IImageRepository, ImageRepository>();
            services.AddTransient<IFileHelper, FileHelper>();
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddScoped<IUserInfoService, UserInfoService>();
            services.AddScoped<IAuthorizationHandler, UserMustBeOwnerRequirementHandler>();
        }

        public static void AddAutoMapper(this IServiceCollection services)
        {
            // Auto Mapper Configurations
            var mappingConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new MappingProfile());
            });

            mappingConfig.AssertConfigurationIsValid();

            IMapper mapper = mappingConfig.CreateMapper();

            services.AddSingleton(mapper);
        }
    }
}
