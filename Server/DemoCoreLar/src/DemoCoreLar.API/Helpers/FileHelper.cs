﻿using DemoCoreLar.API.Dtos;
using DemoCoreLar.API.Entities;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using System;
using System.IO;

namespace DemoCoreLar.API.Helpers
{
    public class FileHelper : IFileHelper
    {
        private readonly IHostingEnvironment _hostingEnvironment;
        public FileHelper(IHostingEnvironment hostingEnvironment)
        {
            _hostingEnvironment = hostingEnvironment;
        }
        
        public string SaveImageToDirectory(IFormFile file)
        {
            var webRootPath = _hostingEnvironment.WebRootPath;

            string fileName = Guid.NewGuid().ToString() + ".jpg";

            var filePath = Path.Combine($"{webRootPath}/images/{fileName}");

            // write bytes and auto-close stream
            using (var stream = new FileStream(filePath, FileMode.Create))
            {
                file.CopyTo(stream);
            }

            return fileName;
        }

        public void DeleteImageUploaded(string fileName)
        {
            var webRootPath = _hostingEnvironment.WebRootPath;

            var filePath = Path.Combine($"{webRootPath}/images/{fileName}");

            System.IO.File.Delete(filePath);
        }

        public void UpdateImageUploaded(ImageForUpdateDto imageForUpdate, Image imageFromRepo)
        {
            if (imageForUpdate.File != null && imageForUpdate.File.Length > 0)
            {
                string fileName = SaveImageToDirectory(imageForUpdate.File);

                DeleteImageUploaded(imageFromRepo.FileName);

                imageFromRepo.FileName = fileName;
            }
        }
    }
}
