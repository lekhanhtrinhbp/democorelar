﻿using DemoCoreLar.API.Dtos;
using DemoCoreLar.API.Entities;
using Microsoft.AspNetCore.Http;

namespace DemoCoreLar.API.Helpers
{
    public interface IFileHelper
    {
        string SaveImageToDirectory(IFormFile file);
        void DeleteImageUploaded(string fileName);

        void UpdateImageUploaded(ImageForUpdateDto imageForUpdate, Image imageFromRepo);
    }
}