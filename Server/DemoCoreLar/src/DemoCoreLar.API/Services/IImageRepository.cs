﻿using DemoCoreLar.API.Entities;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace DemoCoreLar.API.Services
{
    public interface IImageRepository
    {
        Task<IEnumerable<Image>> GetImagesAsync(CancellationToken cancellationToken = default(CancellationToken));
        Task<IEnumerable<Image>> GetImagesByOwnerAsync(string ownerId, CancellationToken cancellationToken = default(CancellationToken));
        Task<bool> IsImageOwnerAsync(Guid id, string ownerId, CancellationToken cancellationToken = default(CancellationToken));
        Task<Image> GetImageAsync(Guid id, CancellationToken cancellationToken = default(CancellationToken));
        Task<bool> ImageExistsAsync(Guid id, CancellationToken cancellationToken = default(CancellationToken));
        Task AddImageAsync(Image image, CancellationToken cancellationToken = default(CancellationToken));
        Task UpdateImageAsync(Image image);
        Task DeleteImageAsync(Image image);
        Task<bool> SaveAsync(CancellationToken cancellationToken = default(CancellationToken));
    }
}
