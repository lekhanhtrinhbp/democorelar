﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using DemoCoreLar.API.Entities;
using Microsoft.EntityFrameworkCore;

namespace DemoCoreLar.API.Services
{
    public class ImageRepository : IImageRepository, IDisposable
    {
        private ImageGalleryContext _context;

        public ImageRepository(ImageGalleryContext imageGalleryContext)
        {
            _context = imageGalleryContext;
        }
        public async Task AddImageAsync(Image image, CancellationToken cancellationToken = default(CancellationToken))
        {
             await _context.Images.AddAsync(image, cancellationToken).ConfigureAwait(false);
        }

        public Task DeleteImageAsync(Image image)
        {
            //_context.Images.Remove(image);
            _context.Entry(image).State = EntityState.Deleted;
            return Task.CompletedTask;
        }

        public async Task<Image> GetImageAsync(Guid id, CancellationToken cancellationToken = default(CancellationToken))
        {
            return await _context.Images.FirstOrDefaultAsync(i => i.Id == id, cancellationToken).ConfigureAwait(false) ;
        }

        public async Task<IEnumerable<Image>> GetImagesAsync(CancellationToken cancellationToken = default(CancellationToken))
        {
            return await _context.Images.OrderBy(i => i.Title).ToListAsync(cancellationToken).ConfigureAwait(false);
        }
        public async Task<IEnumerable<Image>> GetImagesByOwnerAsync(string ownerId, CancellationToken cancellationToken = default(CancellationToken))
        {
            return await _context.Images.Where(i => i.OwnerId == ownerId).OrderBy(i => i.Title).ToListAsync(cancellationToken).ConfigureAwait(false);
        }


        public async Task<bool> ImageExistsAsync(Guid id, CancellationToken cancellationToken = default(CancellationToken))
        {
            return await _context.Images.AnyAsync(i => i.Id == id, cancellationToken).ConfigureAwait(false);
        }

        public async Task<bool> IsImageOwnerAsync(Guid id, string ownerId, CancellationToken cancellationToken = default(CancellationToken))
        {
            return await _context.Images.AnyAsync(i => i.Id == id && i.OwnerId == ownerId, cancellationToken).ConfigureAwait(false);
        }

        public async Task<bool> SaveAsync(CancellationToken cancellationToken = default(CancellationToken))
        {
            return ( await _context.SaveChangesAsync() >= 0);
        }

        public Task UpdateImageAsync(Image image)
        {
            _context.Images.Attach(image);
            _context.Entry(image).State = EntityState.Modified;
            return Task.CompletedTask;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_context != null)
                {
                    _context.Dispose();
                    _context = null;
                }
            }
        }
    }
}
