﻿using Microsoft.EntityFrameworkCore;

namespace DemoCoreLar.API.Entities
{
    public class ImageGalleryContext : DbContext
    {
        public ImageGalleryContext(DbContextOptions<ImageGalleryContext> options)
            : base(options)
        {

        }
        public DbSet<Image> Images { get; set; }
    }
}
