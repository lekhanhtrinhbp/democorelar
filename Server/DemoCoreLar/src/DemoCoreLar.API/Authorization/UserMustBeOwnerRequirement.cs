﻿using Microsoft.AspNetCore.Authorization;

namespace DemoCoreLar.API.Authorization
{
    public class UserMustBeOwnerRequirement : IAuthorizationRequirement
    {
        public string Role { get; private set; }
        public UserMustBeOwnerRequirement(string role)
        {
            Role = role;
        }
    }
}
