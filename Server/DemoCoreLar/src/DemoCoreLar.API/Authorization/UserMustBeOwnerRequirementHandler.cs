﻿using DemoCoreLar.API.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Threading.Tasks;

namespace DemoCoreLar.API.Authorization
{
    public class UserMustBeOwnerRequirementHandler : AuthorizationHandler<UserMustBeOwnerRequirement>
    {
        private readonly IUserInfoService _userInfoService;
        private readonly IImageRepository _imageRepository;

        public UserMustBeOwnerRequirementHandler(IUserInfoService userInfoService, IImageRepository imageRepository)
        {
            _userInfoService = userInfoService;
            _imageRepository = imageRepository;
        }
        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, UserMustBeOwnerRequirement requirement)
        {
            if ((_userInfoService.Role == requirement.Role && !string.IsNullOrEmpty(requirement.Role)) || _userInfoService.Role == "Administrator")
            {
                context.Succeed(requirement);
                return Task.CompletedTask;
            }

            var filterContext = context.Resource as AuthorizationFilterContext;
            if (filterContext  == null)
            {
                context.Fail();
                return Task.CompletedTask;
            }

            var imageId = filterContext.RouteData.Values["id"].ToString();
            if (string.IsNullOrEmpty(imageId))
            {
                context.Fail();
                return Task.CompletedTask;
            }

            if (!Guid.TryParse(imageId, out Guid imageIdAsGuid))
            {
                context.Fail();
                return Task.CompletedTask;
            }
            var isImageOwner = (_imageRepository.IsImageOwnerAsync(imageIdAsGuid, _userInfoService.UserId.ToString()).GetAwaiter().GetResult()) ;
            if (!isImageOwner)
            {
                context.Fail();
                return Task.CompletedTask;
            }

            context.Succeed(requirement);

            return Task.CompletedTask;

        }
    }
}
