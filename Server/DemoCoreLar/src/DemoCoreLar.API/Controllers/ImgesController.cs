﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using DemoCoreLar.API.Dtos;
using DemoCoreLar.API.Entities;
using DemoCoreLar.API.Helpers;
using DemoCoreLar.API.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace DemoCoreLar.API.Controllers
{
    [Route("api/images")]
    [Authorize]
    public class ImgesController : Controller
    {
        private readonly IImageRepository _imageRepository;
        private readonly IUserInfoService _userInfoService;
        private readonly IFileHelper _fileHelper;
        private readonly IMapper _mapper;

        public ImgesController(IImageRepository imageRepository, IUserInfoService userInfoService, IFileHelper fileHelper, IMapper mapper)
        {
            _imageRepository = imageRepository;
            _userInfoService = userInfoService;
            _fileHelper = fileHelper;
            _mapper = mapper;
        }


        [HttpGet()]
        public async Task<IActionResult> GetImages(CancellationToken cancellationToken = default(CancellationToken))
        {
            IEnumerable<Image> imagesFromRepo = new List<Image>();

            if (_userInfoService.Role == "Administrator")
            {
                imagesFromRepo = await _imageRepository.GetImagesAsync(cancellationToken);
            }
            else
            {
                if (!Guid.TryParse(_userInfoService.UserId, out Guid userIdAsGuid))
                {
                    return Forbid();
                }

                imagesFromRepo = await _imageRepository.GetImagesByOwnerAsync(_userInfoService.UserId, cancellationToken);
            }

            var imagesToReturn = _mapper.Map<IEnumerable<ImageDto>>(imagesFromRepo);

            return Ok(imagesFromRepo);
        }

        [HttpGet("{id}", Name = "GetImage")]
        [Authorize(Policy = "UserMustBeOwner")]
        public async Task<IActionResult> GetImage(Guid id, CancellationToken cancellationToken = default(CancellationToken))
        {
            var imageFromRepo = await _imageRepository.GetImageAsync(id, cancellationToken);

            if (imageFromRepo == null)
            {
                return NotFound();
            }

            var imageToReturn = _mapper.Map<ImageDto>(imageFromRepo);

            return Ok(imageToReturn);
        }

        [HttpPost()]
        public async Task<IActionResult> CreateImage([FromForm] ImageForCreationDto image, CancellationToken cancellationToken = default(CancellationToken))
        {
            if (image == null)
            {
                return BadRequest();
            }

            if (!ModelState.IsValid)
            {
                // return 422 - Unprocessable Entity when validation fails
                return new Helpers.UnprocessableEntityObjectResult(ModelState);
            }

            var imageEntity = _mapper.Map<Entities.Image>(image);

            string fileName = _fileHelper.SaveImageToDirectory(image.File);

            // fill out the filename
            imageEntity.FileName = fileName;

            imageEntity.OwnerId = _userInfoService.UserId;

            // add and save.  
            await _imageRepository.AddImageAsync(imageEntity, cancellationToken);

            if (! await _imageRepository.SaveAsync(cancellationToken))
            {
                throw new Exception($"Adding an image failed on save.");
            }

            var imageToReturn = _mapper.Map<ImageDto>(imageEntity);

            return CreatedAtRoute("GetImage", new { id = imageToReturn.Id }, imageToReturn);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteImage(Guid id, CancellationToken cancellationToken = default(CancellationToken))
        {

            var imageFromRepo = await _imageRepository.GetImageAsync(id, cancellationToken);

            if (imageFromRepo == null)
            {
                return NotFound();
            }

            await _imageRepository.DeleteImageAsync(imageFromRepo);

            if (! await _imageRepository.SaveAsync(cancellationToken))
            {
                throw new Exception($"Deleting image with {id} failed on save.");
            }
            else
            {
                _fileHelper.DeleteImageUploaded(imageFromRepo.FileName);
            }

            return NoContent();
        }

        [HttpPut("{id}")]
        [Authorize(Policy = "UserMustBeOwner")]
        public async Task<IActionResult> UpdateImage(Guid id, [FromForm] ImageForUpdateDto imageForUpdate, CancellationToken cancellationToken = default(CancellationToken))
        {
            if (imageForUpdate == null)
            {
                return BadRequest();
            }

            if (!ModelState.IsValid)
            {
                // return 422 - Unprocessable Entity when validation fails
                return new Helpers.UnprocessableEntityObjectResult(ModelState);
            }

            var imageFromRepo = await _imageRepository.GetImageAsync(id, cancellationToken);

            if (imageFromRepo == null)
            {
                return NotFound();
            }

            _mapper.Map(imageForUpdate, imageFromRepo);

            _fileHelper.UpdateImageUploaded(imageForUpdate, imageFromRepo);

            await _imageRepository.UpdateImageAsync(imageFromRepo);

            if (!await _imageRepository.SaveAsync(cancellationToken))
            {
                throw new Exception($"Updating image with {id} failed on save.");
            }

            return NoContent();
        }


    }
}