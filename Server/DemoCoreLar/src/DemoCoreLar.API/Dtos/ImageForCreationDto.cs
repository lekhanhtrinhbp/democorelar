﻿using Microsoft.AspNetCore.Http;
using System.ComponentModel.DataAnnotations;

namespace DemoCoreLar.API.Dtos
{
    public class ImageForCreationDto
    {
        [Required]
        [MaxLength(150)]
        public string Title { get; set; }

        [Required]
        public IFormFile File { get; set; }
    }
}
