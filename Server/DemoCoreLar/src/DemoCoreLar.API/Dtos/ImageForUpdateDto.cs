﻿using Microsoft.AspNetCore.Http;
using System.ComponentModel.DataAnnotations;

namespace DemoCoreLar.API.Dtos
{
    public class ImageForUpdateDto
    {
        [Required]
        [MaxLength(150)]
        public string Title { get; set; }
        public IFormFile File { get; set; }
        public string FileName { get; set; }
    }
}
