﻿using System;

namespace DemoCoreLar.API.Dtos
{
    public class ImageDto
    {
        public Guid Id { get; set; }

        public string Title { get; set; }

        public string FileName { get; set; }
    }
}
