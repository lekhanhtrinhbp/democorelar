﻿using AutoMapper;
using DemoCoreLar.API.Dtos;
using DemoCoreLar.API.Entities;

namespace DemoCoreLar.API.Mapper
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
                // Map from Image (entity) to Image, and back
                CreateMap<Image, ImageDto>().ReverseMap();

                // Map from ImageForCreation to Image
                // Ignore properties that shouldn't be mapped
                CreateMap<ImageForCreationDto, Image>()
                    .ForMember(m => m.FileName, options => options.Ignore())
                    .ForMember(m => m.Id, options => options.Ignore())
                    .ForMember(m => m.OwnerId, options => options.Ignore());

                // Map from ImageForUpdate to Image
                // ignore properties that shouldn't be mapped
                CreateMap<ImageForUpdateDto, Image>()
                    .ForMember(m => m.FileName, options => options.Ignore())
                    .ForMember(m => m.Id, options => options.Ignore())
                    .ForMember(m => m.OwnerId, options => options.Ignore());
        }
    }
}
