using AutoMapper;
using DemoCoreLar.API.Controllers;
using DemoCoreLar.API.Dtos;
using DemoCoreLar.API.Entities;
using DemoCoreLar.API.Helpers;
using DemoCoreLar.API.Mapper;
using DemoCoreLar.API.Services;
using Microsoft.AspNetCore.Mvc;
using Moq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace DemoCoreLar.API.Tests
{
    public class ImageControllerShould
    {
        private readonly Mock<IImageRepository> _mockRepository;
        private readonly Mock<IUserInfoService> _mockUserInfoService;
        private readonly Mock<IFileHelper> _mockIFileHelper;
        private static readonly IList<Image> images = new ReadOnlyCollection<Image>(
            new List<Image>()
            {
                 new Image()
                {
                     Id = new Guid("25320c5e-f58a-4b1f-b63a-8ee07a840bdf"),
                     Title = "An image by Trinh",
                     FileName = "3fbe2aea-2257-44f2-b3b1-3d8bacade89c.jpg",
                     OwnerId = "d860efca-22d9-47fd-8249-791ba61b07c7"
                }
            }
        );

        private readonly ImgesController _sut;

        public ImageControllerShould()
        {
            _mockRepository = new Mock<IImageRepository>();
            _mockUserInfoService = new Mock<IUserInfoService>();
            _mockIFileHelper = new Mock<IFileHelper>();


            var mapperConfiguration = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new MappingProfile());
            });

            var _mockMapper = mapperConfiguration.CreateMapper();

            _sut = new ImgesController(_mockRepository.Object, _mockUserInfoService.Object, _mockIFileHelper.Object, _mockMapper);
        }

        [Fact]
        public async Task ReturnImages_When_UserRoleIsAdministrator()
        {
            _mockUserInfoService.Setup(x => x.Role).Returns("Administrator");
            _mockRepository.Setup(x => x.GetImagesAsync(It.IsAny<CancellationToken>())).ReturnsAsync(images);

            IActionResult result = await _sut.GetImages();


            var okResult = Assert.IsType<OkObjectResult>(result);
            Assert.NotNull(okResult);
            Assert.Equal(200, okResult.StatusCode);
        }

        [Fact]
        public async Task ReturnForbid_When_GetImagesWithInvalidUserId()
        {
            _mockUserInfoService.Setup(x => x.UserId).Returns("");
            IActionResult result = await _sut.GetImages();

            var forbidResult = Assert.IsType<ForbidResult>(result);
            Assert.NotNull(forbidResult);
        }

        [Fact]
        public async Task ReturnImages_When_GetImagesWithValidUserId()
        {
            _mockUserInfoService.Setup(x => x.UserId).Returns(Guid.NewGuid().ToString());
            _mockRepository.Setup(x => x.GetImagesByOwnerAsync(It.IsAny<string>(), It.IsAny<CancellationToken>())).ReturnsAsync(images);

            IActionResult result = await _sut.GetImages();

            var okResult = Assert.IsType<OkObjectResult>(result);
            Assert.NotNull(okResult);
            Assert.Equal(200, okResult.StatusCode);
        }

        [Fact]
        public async Task ReturnImage_When_GetImageWithExistImageId()
        {
            _mockRepository.Setup(x => x.GetImageAsync(It.IsAny<Guid>(), It.IsAny<CancellationToken>())).ReturnsAsync(images[0]);

            IActionResult result = await _sut.GetImage(images[0].Id);

            var okResult = Assert.IsType<OkObjectResult>(result);
            Assert.NotNull(okResult);
            Assert.Equal(200, okResult.StatusCode);
        }

        [Fact]
        public async Task ReturnNotFound_When_GetImageWithImageIdDoesNotExist()
        {
            _mockRepository.Setup(x => x.GetImageAsync(It.IsAny<Guid>(), It.IsAny<CancellationToken>())).ReturnsAsync((Image)null);

            IActionResult result = await _sut.GetImage(images[0].Id);

            var notFoundResult = Assert.IsType<NotFoundResult>(result);
            Assert.NotNull(notFoundResult);
            Assert.Equal(404, notFoundResult.StatusCode);
        }

        [Fact]
        public async Task DeleteImage_When_DeleteValidImageId()
        {
            _mockRepository.Setup(x => x.GetImageAsync(It.IsAny<Guid>(), It.IsAny<CancellationToken>())).ReturnsAsync(new Image());
            _mockRepository.Setup(x => x.DeleteImageAsync(It.IsAny<Image>())).Returns(Task.CompletedTask);
            _mockRepository.Setup(x => x.SaveAsync(It.IsAny<CancellationToken>())).ReturnsAsync(true);
            _mockIFileHelper.Setup(x => x.DeleteImageUploaded(It.IsAny<string>()));

            IActionResult result = await _sut.DeleteImage(Guid.NewGuid());

            var noContentResult = Assert.IsType<NoContentResult>(result);
        }

        [Fact]
        public async Task ReturnNotFound_When_DeleteImageWithImageIdDoesNotExist()
        {
            _mockRepository.Setup(x => x.GetImageAsync(It.IsAny<Guid>(), It.IsAny<CancellationToken>())).ReturnsAsync((Image)null);

            IActionResult result = await _sut.DeleteImage(Guid.NewGuid());

            var noContentResult = Assert.IsType<NotFoundResult>(result);
        }

        [Fact]
        public async Task ThrowException_When_DeleteImageWithRepositorySaveFailed()
        {
            _mockRepository.Setup(x => x.GetImageAsync(It.IsAny<Guid>(), It.IsAny<CancellationToken>())).ReturnsAsync(new Image());
            _mockRepository.Setup(x => x.DeleteImageAsync(It.IsAny<Image>())).Returns(Task.CompletedTask);
            _mockRepository.Setup(x => x.SaveAsync(It.IsAny<CancellationToken>())).ReturnsAsync(false);

            await Assert.ThrowsAsync<Exception>(() =>  _sut.DeleteImage(Guid.NewGuid()));
        }

        [Fact]
        public async Task ReturnsCreateAtRoute_When_CreateImageWithValidImageParameter()
        {
            Image saveImage = null;

            var image = new ImageForCreationDto
            {
                Title = "Test Title"
            };

            var cancelationToken = new CancellationToken();

            _mockRepository.Setup(x => x.AddImageAsync(It.IsAny<Image>(), It.IsAny<CancellationToken>()))
                .Returns(Task.CompletedTask).Callback<Image, CancellationToken>((x, c) => saveImage = x);
            _mockIFileHelper.Setup(x => x.SaveImageToDirectory(image.File)).Returns(Guid.NewGuid().ToString());
            _mockUserInfoService.Setup(x => x.UserId).Returns(Guid.NewGuid().ToString());
            _mockRepository.Setup(x => x.SaveAsync(It.IsAny<CancellationToken>())).ReturnsAsync(true);

            IActionResult result = await _sut.CreateImage(image, cancelationToken);

            _mockRepository.Verify(
                x => x.AddImageAsync(It.IsAny<Image>(), It.IsAny<CancellationToken>()), Times.Once);

            var createdAtRouteResult = Assert.IsType<CreatedAtRouteResult>(result);
            var imageResult = Assert.IsType<ImageDto>(createdAtRouteResult.Value);
            Assert.Equal(saveImage.Title, imageResult.Title);
        }

        [Fact]
        public async Task ReturnBadRequest_When_CreateImageWithNullImageParameter()
        {
            ImageForCreationDto image = null;

            IActionResult result = await _sut.CreateImage(image);

            _mockRepository.Verify(
                x => x.AddImageAsync(It.IsAny<Image>(), It.IsAny<CancellationToken>()), Times.Never);

            var createdAtRouteResult = Assert.IsType<BadRequestResult>(result);
        }

        [Fact]
        public async Task ReturnUnprocessableResult_When_CreateImageWithInvalidImageParameter()
        {
            var image = new ImageForCreationDto();

            _sut.ModelState.AddModelError("Title", "Title is required");

            IActionResult result = await _sut.CreateImage(image);

            _mockRepository.Verify(
                x => x.AddImageAsync(It.IsAny<Image>(), It.IsAny<CancellationToken>()), Times.Never);

            Assert.IsType<Helpers.UnprocessableEntityObjectResult>(result);
        }

        [Fact]
        public async Task ThrowException_When_CreateImageWithRepositorySaveFailed()
        {
            var image = new ImageForCreationDto
            {
                Title = "Test Title"
            };

            var cancelationToken = new CancellationToken();

            _mockRepository.Setup(x => x.AddImageAsync(It.IsAny<Image>(), It.IsAny<CancellationToken>()))
                .Returns(Task.CompletedTask);
            _mockIFileHelper.Setup(x => x.SaveImageToDirectory(image.File)).Returns(Guid.NewGuid().ToString());
            _mockUserInfoService.Setup(x => x.UserId).Returns(Guid.NewGuid().ToString());
            _mockRepository.Setup(x => x.SaveAsync(It.IsAny<CancellationToken>())).ReturnsAsync(false);

            await Assert.ThrowsAsync<Exception>(() => _sut.CreateImage(image, cancelationToken));
            
            _mockRepository.Verify(
                x => x.AddImageAsync(It.IsAny<Image>(), It.IsAny<CancellationToken>()), Times.Once);
           
        }

        [Fact]
        public async Task ReturnsNoContent_When_UpdateImageWithValidImageParameter()
        {
            Image saveImage = null;

            var image = new ImageForUpdateDto
            {
                Title = images[0].Title,
                FileName = images[0].FileName
            };

            _mockRepository.Setup(x => x.GetImageAsync(It.IsAny<Guid>(), It.IsAny<CancellationToken>())).ReturnsAsync(new Image());
            _mockRepository.Setup(x => x.UpdateImageAsync(It.IsAny<Image>())).Returns(Task.CompletedTask).Callback<Image>(x => saveImage = x);
            _mockIFileHelper.Setup(x => x.UpdateImageUploaded(It.IsAny<ImageForUpdateDto>(), It.IsAny<Image>()));
            _mockUserInfoService.Setup(x => x.UserId).Returns(Guid.NewGuid().ToString());
            _mockRepository.Setup(x => x.SaveAsync(It.IsAny<CancellationToken>())).ReturnsAsync(true);

            IActionResult result = await _sut.UpdateImage(images[0].Id, image);

            _mockRepository.Verify(
                x => x.UpdateImageAsync(It.IsAny<Image>()), Times.Once);

            var createdAtRouteResult = Assert.IsType<NoContentResult>(result);
        }

        [Fact]
        public async Task ReturnBadRequest_When_UpdateImageWithNullImageParameter()
        {
            ImageForUpdateDto image = null;

            IActionResult result = await _sut.UpdateImage(Guid.NewGuid(), image);

            _mockRepository.Verify(
                x => x.UpdateImageAsync(It.IsAny<Image>()), Times.Never);

            var createdAtRouteResult = Assert.IsType<BadRequestResult>(result);
        }

        [Fact]
        public async Task ReturnUnprocessableResult_When_UpdateImageWithInvalidImageParameter()
        {
            var image = new ImageForUpdateDto();

            _sut.ModelState.AddModelError("Title", "Title is required");

            IActionResult result = await _sut.UpdateImage( Guid.NewGuid(), image);

            _mockRepository.Verify(
                x => x.AddImageAsync(It.IsAny<Image>(), It.IsAny<CancellationToken>()), Times.Never);

            Assert.IsType<Helpers.UnprocessableEntityObjectResult>(result);
        }

        [Fact]
        public async Task ThrowException_WhenUpdateImageWithRepositorySaveFailed()
        {
            Image saveImage = null;

            var image = new ImageForCreationDto
            {
                Title = "Test Title"
            };

            _mockRepository.Setup(x => x.GetImageAsync(It.IsAny<Guid>(), It.IsAny<CancellationToken>())).ReturnsAsync(new Image());
            _mockRepository.Setup(x => x.UpdateImageAsync(It.IsAny<Image>())).Callback<Image>(x => saveImage = x);
            _mockIFileHelper.Setup(x => x.UpdateImageUploaded(It.IsAny<ImageForUpdateDto>(), It.IsAny<Image>()));
            _mockUserInfoService.Setup(x => x.UserId).Returns(Guid.NewGuid().ToString());
            _mockRepository.Setup(x => x.SaveAsync(It.IsAny<CancellationToken>())).ReturnsAsync(false);

            await Assert.ThrowsAsync<Exception>(() => _sut.CreateImage(image));

            _mockRepository.Verify(
                x => x.AddImageAsync(It.IsAny<Image>(), It.IsAny<CancellationToken>()), Times.Once);

        }

    }
}
