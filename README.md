# DemoCoreLar
## Description:
This is a tiny image gallery application. I use OpenID Connect and OAuth 2 protocols to authenticate users and authorize for Angular app access to functionality and data in ASP.NET Core API.

## Server(api):
### Technical stack:
- Asp.net Core 2.2.
- EntityFramework Core 2.2 Code first.
- IdentityServer4.
- AutoMapper.
### Setup guideline:
1. Make sure .NET core SDK 2.2 is installed
2. At the server folder run the following command to install dependencies:
    ```
    dotnet restore
    ```    
3. Setup Startup Project:
    Right click on solution -> Setup Starup Project -> Common Property -> Startup Project
    Choose Multiple Startup Project -> Select Action Start for  API and IdP project

### DemoCoreLar API Swagger:
- [https://localhost:44372/swagger/index.html](https://localhost:44372/swagger/index.html)

## Client:
### Technical stack:
- Angular 7.
- Typescript.
- Angualar Reactive Form.
### Setup guideline:
1. Make sure nodejs & npm are installed.
2. At the root folder of the client project run the following command to install dependencies:
    ```
    npm install
    ```
3. Enable SSL for client:
    I already attach SSL key and cert in folder ssl. See [this article ](https://medium.com/@rubenvermeulen/running-angular-cli-over-https-with-a-trusted-certificate-4a0d5f92747a) and install SSL for angualr app
4. Start the front-end application:
    ```
    npm start
    ```
### Try result:
#### Login:
- Use these username/password to login
    ```
    Admin: admin/password
    Free user: Frank/password
    ```  